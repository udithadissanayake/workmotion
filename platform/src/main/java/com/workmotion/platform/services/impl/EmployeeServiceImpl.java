package com.workmotion.platform.services.impl;


import com.workmotion.platform.enums.ProcessEvents;
import com.workmotion.platform.model.Employee;
import com.workmotion.platform.repository.EmployeeRepository;
import com.workmotion.platform.services.EmployeeService;
import com.workmotion.platform.utils.EmployeeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final static Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PersistStateMachineHandler persistStateMachineHandler;

    @Override
    public List<Employee> getEmployees() {
        return (List<Employee>) employeeRepository.findAll();
    }

    @Override
    public Employee createEmployee(Employee employee) {
        logger.info("Employee Created successfully");
        return employeeRepository.save(employee);
    }

    @Override
    public boolean updateEmployeeStatus(Long id, ProcessEvents events) {
        Optional<Employee> employee = employeeRepository.findById(id);
        boolean status=false;
        try {

             Message<String> event = MessageBuilder.withPayload(events.name()).setHeader(EmployeeConstants.EmployeeHeader, employee.get()).build();
             String field = employee.get().getState().name();
             status = persistStateMachineHandler.handleEventWithState(
                    event, field
            );
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return status;
    }
}
