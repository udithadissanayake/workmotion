package com.workmotion.platform.services;

import com.workmotion.platform.enums.ProcessEvents;
import com.workmotion.platform.model.Employee;

import java.util.List;

public interface EmployeeService {
    public List<Employee> getEmployees();
    public Employee createEmployee(Employee employee);
    public boolean updateEmployeeStatus(Long id, ProcessEvents events);
}
