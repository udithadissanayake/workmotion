package com.workmotion.platform.model;

import com.workmotion.platform.enums.ProcessState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@javax.persistence.Entity
@Table(name = "security")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Security {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "employeeId")
    private Long employeeId;

    @Column(name = "sec_check_state")
    @Enumerated(value = EnumType.STRING)
    private ProcessState securityCheckState;


}
