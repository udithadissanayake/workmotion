package com.workmotion.platform.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.workmotion.platform.enums.ProcessState;
import org.springframework.data.annotation.Persistent;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;


@javax.persistence.Entity
@Table(name = "employee")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class Employee {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "firstName", nullable = false, unique = true)
    private String firstName;

    @Column(name = "lastName", nullable = false)
    private String lastName;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private ProcessState state;

    @Column(name = "sec_check_state")
    @Enumerated(value = EnumType.STRING)
    private ProcessState securityCheckState;

    @Column(name = "work_permit_state")
    @Enumerated(value = EnumType.STRING)
    private ProcessState workPermitState;

}
