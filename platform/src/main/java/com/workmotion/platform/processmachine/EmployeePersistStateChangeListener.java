package com.workmotion.platform.processmachine;

import com.workmotion.platform.enums.ProcessState;
import com.workmotion.platform.model.Employee;
import com.workmotion.platform.repository.EmployeeRepository;
import com.workmotion.platform.utils.EmployeeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler.PersistStateChangeListener;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

@Component
public class EmployeePersistStateChangeListener implements PersistStateChangeListener {

    private final static Logger logger = LoggerFactory.getLogger(EmployeePersistStateChangeListener.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void onPersist(State<String, String> state,
                          Message<String> message,
                          Transition<String, String> transition,
                          StateMachine<String, String> stateMachine) {
        if (message != null && message.getHeaders().containsKey(EmployeeConstants.EmployeeHeader)) {
            Employee employee = message.getHeaders().get(EmployeeConstants.EmployeeHeader, Employee.class);

            if(ProcessState.valueOf(state.getId()).equals(ProcessState.SECURITY_CHECK_FINISHED)
                || ProcessState.valueOf(state.getId()).equals(ProcessState.SECURITY_CHECK_STARTED)){
                employee.setSecurityCheckState(ProcessState.valueOf(state.getId()));
                employee.setState(ProcessState.valueOf(ProcessState.INCHECK.name()));
            }

            if(ProcessState.valueOf(state.getId()).equals(ProcessState.WORK_PERMIT_CHECK_FINISHED)
                || ProcessState.valueOf(state.getId()).equals(ProcessState.WORK_PERMIT_CHECK_STARTED)){
                employee.setWorkPermitState(ProcessState.valueOf(state.getId()));
                employee.setState(ProcessState.valueOf(ProcessState.INCHECK.name()));
            }


                if(employee.getWorkPermitState() != null && employee.getSecurityCheckState() != null
                        &&  employee.getWorkPermitState().equals(ProcessState.WORK_PERMIT_CHECK_FINISHED)
                && employee.getSecurityCheckState().equals(ProcessState.SECURITY_CHECK_FINISHED) ){
                    employee.setState(ProcessState.valueOf(ProcessState.APPROVED.name()));
                }else {
                    employee.setState(ProcessState.valueOf(state.getId()));
                }

            logger.debug("Employee Created {}", employee);
            employeeRepository.save(employee);
        }
    }
}
