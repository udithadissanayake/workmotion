package com.workmotion.platform.processmachine;

import com.workmotion.platform.enums.ProcessEvents;
import com.workmotion.platform.enums.ProcessState;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableStateMachine(name = "employeeStateMachine")
public class EmployeeStateMachineConfig  extends StateMachineConfigurerAdapter<String, String> {

    @Override
    public void configure(StateMachineStateConfigurer<String, String> states)
            throws Exception {
        Set<String> stringStates = new HashSet<>();
        EnumSet.allOf(ProcessState.class).forEach(entity -> stringStates.add(entity.name()));
        states.withStates()
                .initial(ProcessState.ADDED.name())
                .state(ProcessState.INCHECK.name())
                .join(ProcessState.APPROVED.name())
                .end(ProcessState.ACTIVE.name())
                .states(stringStates);
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(ProcessState.ADDED.name()).target(ProcessState.INCHECK.name()).event(ProcessEvents.CHECK_STARTED.name())

                .and()
                .withExternal()
                .source(ProcessState.INCHECK.name()).target(ProcessState.SECURITY_CHECK_STARTED.name()).event(ProcessEvents.SEC_CHECK_STARTED.name())

                .and()
                .withExternal()
                .source(ProcessState.WORK_PERMIT_CHECK_STARTED.name()).target(ProcessState.SECURITY_CHECK_STARTED.name()).event(ProcessEvents.SEC_CHECK_STARTED.name())

                .and()
                .withExternal()
                .source(ProcessState.WORK_PERMIT_CHECK_FINISHED.name()).target(ProcessState.SECURITY_CHECK_STARTED.name()).event(ProcessEvents.SEC_CHECK_STARTED.name())

                .and()
                .withExternal()
                .source(ProcessState.INCHECK.name()).target(ProcessState.WORK_PERMIT_CHECK_STARTED.name()).event(ProcessEvents.WORK_PER_STARTED.name())

                .and()
                .withExternal()
                .source(ProcessState.SECURITY_CHECK_STARTED.name()).target(ProcessState.WORK_PERMIT_CHECK_STARTED.name()).event(ProcessEvents.WORK_PER_STARTED.name())

                .and()
                .withExternal()
                    .source(ProcessState.SECURITY_CHECK_STARTED.name()).target(ProcessState.SECURITY_CHECK_FINISHED.name()).event(ProcessEvents.SEC_CHECK_FINISHED.name())

                .and()
                .withExternal()
                .source(ProcessState.WORK_PERMIT_CHECK_STARTED.name()).target(ProcessState.SECURITY_CHECK_FINISHED.name()).event(ProcessEvents.SEC_CHECK_FINISHED.name())

                .and()
                .withExternal()
                .source(ProcessState.WORK_PERMIT_CHECK_FINISHED.name()).target(ProcessState.SECURITY_CHECK_FINISHED.name()).event(ProcessEvents.SEC_CHECK_FINISHED.name())

                .and()
                .withExternal()
                .source(ProcessState.WORK_PERMIT_CHECK_STARTED.name()).target(ProcessState.WORK_PERMIT_CHECK_FINISHED.name()).event(ProcessEvents.WORK_PER_FINISHED.name())

                .and()
                .withExternal()
                .source(ProcessState.SECURITY_CHECK_FINISHED.name()).target(ProcessState.WORK_PERMIT_CHECK_FINISHED.name()).event(ProcessEvents.WORK_PER_FINISHED.name())

                .and()
                .withExternal()
                .source(ProcessState.APPROVED.name()).target(ProcessState.ACTIVE.name()).event(ProcessEvents.USER_ACTIVATED.name())

                .and()
                .withJoin()
                    .source(ProcessState.SECURITY_CHECK_FINISHED.name())
                    .source(ProcessState.WORK_PERMIT_CHECK_FINISHED.name())
                    .target(ProcessState.APPROVED.name())

                .and()
                .withExternal()
                .source(ProcessState.APPROVED.name()).target(ProcessState.ACTIVE.name())
                .event(ProcessEvents.USER_ACTIVATED.name());
    }
}
