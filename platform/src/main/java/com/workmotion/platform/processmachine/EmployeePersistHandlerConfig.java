package com.workmotion.platform.processmachine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;

@Configuration
public class EmployeePersistHandlerConfig {

    @Autowired
    private StateMachine<String, String> employeeStateMachine;

    @Autowired
    private EmployeePersistStateChangeListener entityPersistStateChangeListener;

    @Bean
    public PersistStateMachineHandler persistStateMachineHandler() {
        PersistStateMachineHandler handler = new PersistStateMachineHandler(employeeStateMachine);
        handler.addPersistStateChangeListener(entityPersistStateChangeListener);
        return handler;
    }

}
