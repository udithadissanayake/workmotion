package com.workmotion.platform.controller;


import com.workmotion.platform.enums.ProcessEvents;
import com.workmotion.platform.model.Employee;
import com.workmotion.platform.services.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("employee")
public class EmployeeController extends AbstractController {

    private final static Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = "/")
    public List<Employee> getEmployees() {

        logger.info("Request received for employee list");
        return employeeService.getEmployees();
    }

    @RequestMapping(value = "/",method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody Employee employee) {

        logger.info("Request received for employee creation");
        return employeeService.createEmployee(employee);
    }

    @RequestMapping(value = "/{id}/status/{event}", method = RequestMethod.PUT)
    public Boolean sendEvent(@PathVariable("id") Long id, @PathVariable("event") ProcessEvents event) {
        return employeeService.updateEmployeeStatus(id, event);
    }

}

