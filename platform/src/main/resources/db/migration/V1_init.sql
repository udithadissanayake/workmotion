CREATE TABLE `employee` (
                          `id`    INT PRIMARY KEY AUTO_INCREMENT,
                          `firstName`  VARCHAR(255) NOT NULL UNIQUE,
                          `lastName`  VARCHAR(255) NOT NULL,
                          `state` VARCHAR(255) NOT NULL,
                          `sec_check_state` VARCHAR(255) NOT NULL,
                          `work_permit_state` VARCHAR(255) NOT NULL
);

CREATE TABLE `security` (
                            `id`    INT PRIMARY KEY AUTO_INCREMENT,
                            `employeeId`  VARCHAR(255) NOT NULL UNIQUE,
                            `sec_check_state` VARCHAR(255) NOT NULL,
);